import pvegas
def v0_to_v1(fin, fout, itmx, maxbins=100):
    ndim, it, ndo, si, swgt, schi, \
    sums, sumsq, xi, pconfig = pvegas.load_file(fin, maxbins)
    
    sums  = sums  * float(itmx)
    sumsq = sumsq * float(itmx)**2

    pvegas.write_file(
        fout, maxbins,
        ndim,it, ndo, si, swgt, schi, 
        sums,sumsq,xi,pconfig
    )

import sys
v0_to_v1(sys.argv[1], sys.argv[2], int(sys.argv[3]))
