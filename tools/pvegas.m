(* ::Package:: *)

loadGrid[filename_, maxbins_: 50] := 
  Block[{stream, ndim, it, ndo, si, swgt, schi, grid, initalised, Y, 
    Y2, err, plotdata, l, d, u, m, e, c,i,j,
    uppers, lowers, deltas, titles, uppersY, lowersY, deltasY,
    mat, mats, nonzero, n0, nbinY, dens},
   
   
   stream = OpenRead[filename, BinaryFormat -> True];
   SetStreamPosition[stream, 0];
   Skip[stream, "Byte", 4];
   ndim = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   it = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   ndo = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   si = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   swgt = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   schi = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   grid = 
    ArrayReshape[
     BinaryRead[stream, Table["Real64", {i, ndim*50}]], {ndim, 50}];
   Skip[stream, "Byte", 8];
   initalised = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   Y = ArrayReshape[
     BinaryRead[stream, 
      Table["Real64", {i, maxbins*initalised}]], {initalised, 
      maxbins}];
   Skip[stream, "Byte", 8];
   Y2 = ArrayReshape[
     BinaryRead[stream, 
      Table["Real64", {i, maxbins*initalised}]], {initalised, 
      maxbins}];
   Skip[stream, "Byte", 8];
   uppers = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   lowers = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   deltas = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   titles = BinaryRead[stream, 
      Table["Character8", {i, initalised*100}]];
   titles = StringTrim@(StringJoin @@@ 
     ArrayReshape[titles, {initalised, 100}]);
   Skip[stream, "Byte", 8];
   uppersY = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   lowersY = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   deltasY = BinaryRead[stream, Table["Real64", {i, initalised}]];

   Close[stream];
   
   m = si/swgt;
   e = Sqrt[1/swgt];
   c = (schi - si^2/swgt)/(it - 1); 

   Y = Y / it;
   err = Sqrt[(Y2 - it Y^2 )/it/(it-1)];
   
   plotdata = 
    Table[Table[{lowers[[i]] + deltas[[i]] (j - 1/2),  Y[[i]][[j]], 
      err[[i]][[j]] }, {j, 
     Ceiling[(uppers[[i]] - lowers[[i]])/deltas[[i]]]}], {i, 
    Length[Y]}];


   mats = {}; dens = {};
   nonzero = Position[deltasY, _?(# != 0 &)] // Flatten;
   n0 = 0;
   nbinY = Ceiling[(uppersY - lowersY)/(deltasY + $MachineEpsilon)];
   While[Length[Select[nonzero, ## > n0 &]] > 0,
    n0 = First[Select[nonzero, ## > n0 &]];
    n0 = n0 + nbinY[[n0]];
    (*mat = Drop[plotdata[[n0 - nbinY[[n0]] ;; n0 - 1]], 0,0,{1,3,2}]/.{i_}->i;*)
	mat = Drop[plotdata[[n0 - nbinY[[n0]] ;; n0 - 1]], 0,0,{1}];
    AppendTo[mats, mat];
    AppendTo[dens, Flatten[Table[{lowers[[n0]] + deltas[[n0]] (j - 1/2), 
       lowersY[[n0]] + deltasY[[n0]] (i - 1/2),  mat[[i]][[j]][[1]],mat[[i]][[j]][[2]]}, {i, 
       Dimensions[mat][[1]]}, {j, Dimensions[mat][[2]]}], 1]];
   ];
   plotdata = If[Length[nonzero] > 0,  Drop[plotdata, {nonzero[[1]], nonzero[[-1]]}], plotdata]; 
   
   If[$VersionNumber > 10,
    return = <|mean -> m, 
       error -> e, \[Chi] -> c, names -> titles, data -> plotdata, vegas -> grid,
       density -> dens |>;
    ,
    return[data] = plotdata;
    return[mean] = m;
    return[error] = e;
    return[names] = titles;
    return[\[Chi]] = c;
    ];
   return
   ];


(* This is deprecated and will be removed *)
loadGridV0[filename_, itmx_:1, maxbins_: 50] := 
  Block[{stream, ndim, it, ndo, si, swgt, schi, grid, initalised, Y, 
    Y2, err, plotdata, l, d, u, m, e, c,i,j,
    uppers, lowers, deltas, titles, uppersY, lowersY, deltasY,
    mat, mats, nonzero, n0, nbinY, dens},
   
   
   stream = OpenRead[filename, BinaryFormat -> True];
   SetStreamPosition[stream, 0];
   Skip[stream, "Byte", 4];
   ndim = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   it = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   ndo = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   si = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   swgt = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   schi = BinaryRead[stream, "Real64"];
   Skip[stream, "Byte", 8];
   grid = 
    ArrayReshape[
     BinaryRead[stream, Table["Real64", {i, ndim*50}]], {ndim, 50}];
   Skip[stream, "Byte", 8];
   initalised = BinaryRead[stream, "Integer32"];
   Skip[stream, "Byte", 8];
   Y = ArrayReshape[
     BinaryRead[stream, 
      Table["Real64", {i, maxbins*initalised}]], {initalised, 
      maxbins}];
   Skip[stream, "Byte", 8];
   Y2 = ArrayReshape[
     BinaryRead[stream, 
      Table["Real64", {i, maxbins*initalised}]], {initalised, 
      maxbins}];
   Skip[stream, "Byte", 8];
   uppers = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   lowers = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   deltas = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   titles = BinaryRead[stream, 
      Table["Character8", {i, initalised*100}]];
   titles = StringTrim@(StringJoin @@@ 
     ArrayReshape[titles, {initalised, 100}]);
   Skip[stream, "Byte", 8];
   uppersY = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   lowersY = BinaryRead[stream, Table["Real64", {i, initalised}]];
   Skip[stream, "Byte", 8];
   deltasY = BinaryRead[stream, Table["Real64", {i, initalised}]];

   Close[stream];
   
   m = si/swgt;
   e = Sqrt[1/swgt];
   c = (schi - si^2/swgt)/(it - 1); 
   err = Sqrt[(-(it*Y^2) + it^2*Y2)/((-1 + it)*it)];
   
   plotdata = 
    Table[Table[{lowers[[i]] + deltas[[i]] (j - 1/2), (itmx/it) Y[[i]][[j]], 
     (itmx/it)^2 err[[i]][[j]]}, {j, 
     Ceiling[(uppers[[i]] - lowers[[i]])/deltas[[i]]]}], {i, 
    Length[Y]}];


   mats = {}; dens = {};
   nonzero = Position[deltasY, _?(# != 0 &)] // Flatten;
   n0 = 0;
   nbinY = Ceiling[(uppersY - lowersY)/(deltasY + $MachineEpsilon)];
   While[Length[Select[nonzero, ## > n0 &]] > 0,
    n0 = First[Select[nonzero, ## > n0 &]];
    n0 = n0 + nbinY[[n0]];
    (*mat = Drop[plotdata[[n0 - nbinY[[n0]] ;; n0 - 1]], 0,0,{1,3,2}]/.{i_}->i;*)
	mat = Drop[plotdata[[n0 - nbinY[[n0]] ;; n0 - 1]], 0,0,{1}];
    AppendTo[mats, mat];
    AppendTo[dens, Flatten[Table[{lowers[[n0]] + deltas[[n0]] (j - 1/2), 
       lowersY[[n0]] + deltasY[[n0]] (i - 1/2),  mat[[i]][[j]][[1]],mat[[i]][[j]][[2]]}, {i, 
       Dimensions[mat][[1]]}, {j, Dimensions[mat][[2]]}], 1]];
   ];
   plotdata = If[Length[nonzero] > 0,  Drop[plotdata, {nonzero[[1]], nonzero[[-1]]}], plotdata]; 
   
   If[$VersionNumber > 10,
    return = <|mean -> m, 
       error -> e, \[Chi] -> c, names -> titles, data -> plotdata, 
       density -> dens |>;
    ,
    return[data] = plotdata;
    return[mean] = m;
    return[error] = e;
    return[names] = titles;
    return[\[Chi]] = c;
    ];
   return
   ];



(* Plot related *)
ClearAll[CombinePlots]
CombinePlots::tdlen = "Objects of unequal length";
CombinePlots::wrongx = "X values do not match";

Options[CombinePlots] = {op ->  Plus};


(*
CombinePlots[A_, B_, OptionsPattern[]] := Block[{opE,x1,x2},
 opE[\[Sigma]1_,y1_,\[Sigma]2_,y2_]:=Sqrt[D[OptionValue[op][x1,x2],x1]^2 \[Sigma]1^2 + D[OptionValue[op][x1,x2],x2]^2 \[Sigma]2^2] /.{x1 -> y1, x2 -> y2};
 If[Length[A] != Length[B], Message[CombinePlots::tdlen]; $Failed, 
  If[Plus @@ (Drop[A, 0, {2, 3}] - Drop[B, 0, {2, 3}] // Flatten) != 
    0, Message[CombinePlots::wrongx]; $Failed, 
   Table[{
		A[[i]][[1]], 
		OptionValue[op][A[[i]][[2]], B[[i]][[2]]], 
		opE[A[[i]][[3]], A[[i]][[2]], B[[i]][[3]], B[[i]][[2]]]}, {i, Length[A]}]]]];
*)
CombinePlots[A_, B_, OptionsPattern[]] := Block[{opE,x1,x2},
	opE[\[Sigma]1_,y1_,\[Sigma]2_,y2_]:=Sqrt[D[OptionValue[op][x1,x2],x1]^2 \[Sigma]1^2 + D[OptionValue[op][x1,x2],x2]^2 \[Sigma]2^2] /.{x1 -> y1, x2 -> y2};
	Select[Table[
		Table[
			If[
				A[[i]][[1]]== B[[j]][[1]],
				{
					A[[i]][[1]],
					OptionValue[op][A[[i]][[2]],B[[j]][[2]]],
					opE[A[[i]][[3]], A[[i]][[2]], B[[j]][[3]], B[[j]][[2]]]
				},
				##&[]
			],
			{j,Length[B]}
		],{i,Length[A]}
	]/.{x_}->x,UnsameQ[#,{}]&]
];


ClearAll[ApplyPlot]
ApplyPlot[A_, fY_] := Block[{x,dummy, opE},
  opE[\[Sigma]_,y_]:=Abs[D[fY[x],x]] \[Sigma]/.x-> y;
  Table[{A[[i]][[1]], fY[A[[i]][[2]]], opE[A[[i]][[3]], A[[i]][[2]]]}, {i, 
    Length[A]}]
  ];



(* Density related *)
ClearAll[ApplyDensity]
ApplyDensity[A_, op_] := Block[{x,dummy, opE},
  opE[\[Sigma]_,y_]:=Abs[D[op[x],x]] \[Sigma]/.x-> y;
  Table[{A[[i]][[1]], A[[i]][[2]], op[A[[i]][[3]]], opE[A[[i]][[4]], A[[i]][[3]]]}, {i, 
    Length[A]}]
  ];


ClearAll[CombineDensity]
CombineDensity::tdlen = "Objects of unequal length";
CombineDensity::wrongx = "X values do not match";
CombineDensity::wrongx = "Y values do not match";

Options[CombineDensity] = {op ->  Plus};

CombineDensity[A_, B_, OptionsPattern[]] := Block[{opE,x1,x2},
 opE[\[Sigma]1_,y1_,\[Sigma]2_,y2_]:=Sqrt[D[OptionValue[op][x1,x2],x1]^2 \[Sigma]1^2 + D[OptionValue[op][x1,x2],x2]^2 \[Sigma]2^2] /.{x1 -> y1, x2 -> y2};
 If[Length[A] != Length[B], Message[CombineDensity::tdlen]; $Failed, 
  If[Plus @@ (Drop[A, 0, {2, 4}] - Drop[B, 0, {2, 4}] // Flatten) != 
    0, Message[CombineDensity::wrongx]; $Failed, 
	If[Plus @@ (Drop[Drop[A, 0, {3, 4}],1] - Drop[Drop[B, 0, {3, 4}],1] // Flatten)!= 
    0, Message[CombineDensity::wrongy]; $Failed, 
   Table[{
		A[[i]][[1]], 
		A[[i]][[2]], 
		OptionValue[op][A[[i]][[3]], B[[i]][[3]]], 
		opE[A[[i]][[4]], A[[i]][[3]], B[[i]][[4]], B[[i]][[3]]]}, {i, Length[A]}]]]]];


(* Plotting stuff *)
ListLogPointPlot3D[d_]:=Module[{data},
	data=d;
	data[[All,3]]=Log10[data[[All,3]]];
	verticalticks={#,10^#//N}&/@Range[
		Floor[Min[data[[All,3]]/.-Infinity-> Indeterminate/.Indeterminate-> Infinity]],
		Ceiling[Max[data[[All,3]]/.Infinity-> Indeterminate/.Indeterminate-> -Infinity]],
		1
	];
	ListPointPlot3D[data,Ticks->{Automatic,Automatic,verticalticks}]
];

ListLogPlot3D[d_]:=Module[{data},
	data=d;
	data[[All,3]]=Log10[data[[All,3]]];
	verticalticks={#,10^#//N}& /@ Range[Floor[Min[data[[All,3]]/.-Infinity-> Indeterminate/.Indeterminate-> Infinity]], Ceiling[Max[data[[All,3]]/.Infinity-> Indeterminate/.Indeterminate-> -Infinity]],1];
	ListPlot3D[data,Ticks->{Automatic,Automatic,verticalticks}]
];

SliceDensity[d_,plot_:ErrorListLogPlot]:=Manipulate[
	plot[
		Select[d,#[[1]]==x&][[All,2;;4]],
		PlotRange -> {
			{d[[All,2]]//Min,d[[All,2]]//Max},
			{Min[Select[d[[All,3]],#!=0&]],Max[Select[d[[All,3]],#!=0&]]}
		}
	],
	{x,d[[All,1]]//Min,d[[All,1]]//Max,d[[All,1]]//Differences//Max}
]


VisualiseGrid[grid_,map_]:=Module[{x,y},
x=Table[map[grid[[All,i]]],{i,49}];
y=-Differences[x]^-1;
ListPlot[Table[{x[[i]],y[[i]]},{i,Min[Length[x],Length[y]]}]]
];
