      PROGRAM TEST1
        use phase_space
        use pvegas
        use vegas_m, only: oldvegas=>vegas,oldvegas1=>vegas1
        implicit none
        real(prec) avgi,sd,chi2a, integrand, avgi2, sd2, chi2a2
        integer ranseed
        integer counter, uni
      
        ranseed = 945945
        
        OPEN(unit=8, FILE='old.tmp',action='write',ERR=996)
        OPEN(unit=9, FILE='new.tmp',err=998,action='write')
        
        uni = 8
        call oldvegas(11,50000,2,ps6er,avgi2,sd2,chi2a2,ranseed)

        uni = 9
        counter = 0
        ranseed = 945945
        call vegas   (11,50000,2,ps6er,avgi,sd,            &
                chi2a,ranseed,'tet.grid',83)
        

        write(0,*)avgi,sd
        write(0,*)avgi2,sd2
        

        close(unit=8,err=999)
        close(unit=9,err=997)


        stop
996     print*,"Could not open old.txt"
        stop
997     print*,"Could not close old.txt"
        stop
998     print*,"Could not open new.txt"
        stop
999     print*,"Could not close new.txt"
        stop

       contains
      
      function ps6er(xx,wt,ndim)
        use pvegas, only: prec
        implicit none
        integer ndim
        real(prec) xx(ndim), wt,Mm,Me, weight, ps6er
        real(prec) p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)


        parameter(Mm = 105.658372)   ! MeV
        !parameter(Me = 0.511     )   ! MeV
        parameter(Me = 10     )   ! MeV

        if (wt.gt.1e-30)        counter = counter + 1
        call ps_6(xx,p1,Mm,p2,Me,p3,Me,p4,Me,p5,0._prec,p6,0._prec,weight)
        ps6er = weight
        write(uni,*) counter,xx, weight, wt

      end function
        



      FUNCTION COS_TH(k1,k2)
        use pvegas, only: ki=>prec
        real (kind=ki), intent(in) :: k1(4), k2(4)
        real (kind=ki) ::  mag1, mag2,COS_TH

        mag1 = sqrt(k1(1)**2 + k1(2)**2 + k1(3)**2)
        mag2 = sqrt(k2(1)**2 + k2(2)**2 + k2(3)**2)

        cos_th = sum(k1(1:3)*k2(1:3))/mag1/mag2

        END FUNCTION COS_TH


    end program

