      PROGRAM TEST2
        use phase_space
        use pvegas
        use vegas_m, only: oldvegas=>vegas,oldvegas1=>vegas1
        implicit none
        real(prec) avgi,sd,chi2a, integrand, avgi2, sd2, chi2a2
        integer ranseed
        integer counter, uni
      

        ranseed = 945945
        call oldvegas(5,10000,10,M2ENN,avgi2,sd2,chi2a2,ranseed)
        call oldvegas1(5,1000000,20,M2ENN,avgi2,sd2,chi2a2,ranseed)

        ranseed = 945945

        ! Preconditioning
        call vegas(5,10000,10,M2ENN,avgi,sd,            &
                chi2a,ranseed,'tet.grid',82)

        ! Running
        call vegas(5,1000000,20,M2ENN,avgi,sd,          &
                chi2a,ranseed,'tet.grid',50)
        
        write(0,*)avgi,sd
        write(0,*)avgi2,sd2
       contains
       
      function M2ENN(xx, wt, ndim)
        use pvegas, only:prec
        implicit none
        integer ndim
        real(prec) xx(ndim), wt
        real(prec) p1(4), p2(4), p3(4), p4(4), n(4)
        real(prec) Mm, Me, weight, wg, M2ENN
        real(prec) s13, s24, s3n
        
        parameter(Mm = 105.658372)   ! MeV
        parameter(Me = 0.0       )   ! MeV

        call ps_4(xx(1:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
        
        s13 = s(p1,p3)
        s24 = s(p2,p4)
        
        n = (/ 0, 0, 1, 0/)
        s3n = s(p3, n)
        m2enn = 2*s13*s24 - 2*Mm*s24*s3n
        m2enn = 8.*m2enn

        m2enn = m2enn*weight*0.5/Mm

        wg = m2enn*wt

      end function


      FUNCTION COS_TH(k1,k2)
        use pvegas, only: ki=>prec
        real (kind=ki), intent(in) :: k1(4), k2(4)
        real (kind=ki) ::  mag1, mag2,COS_TH

        mag1 = sqrt(k1(1)**2 + k1(2)**2 + k1(3)**2)
        mag2 = sqrt(k2(1)**2 + k2(2)**2 + k2(3)**2)

        cos_th = sum(k1(1:3)*k2(1:3))/mag1/mag2

        END FUNCTION COS_TH


    end program

