#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include "config.h"
#include <assert.h>

#if HAVE_SIGNAL_H==1
#include <signal.h>
#endif

#ifdef HAVE_ERRNO_H
#ifdef HAVE_STRINGS_H
#include <errno.h>
#include <string.h>
#endif
#endif


typedef long long int memindex;
typedef struct { void *addr; int id; } shminfo;
void FC_FUNC(shmalloc,SHMALLOC)
           (shminfo *base, memindex *i, const int *n, const int *size)
{
    base->id = shmget(IPC_PRIVATE, *size*(*n + 1) - 1, IPC_CREAT | 0600);

#ifdef HAVE_ERRNO_H
#ifdef HAVE_STRINGS_H
    if (base->id == -1)
        printf("Could not get shared memory: %s\n", strerror(errno));
#endif
#endif
    assert(base->id != -1);

    base->addr = shmat(base->id, NULL, 0);

    assert(base->addr != (void *)-1);
    *i = ((char *)(base->addr + *size - 1) - (char *)base)/(long)*size;
}
void FC_FUNC(shmfree,SHMFREE)(shminfo *base)
{
    shmdt(base->addr);
    shmctl(base->id, IPC_RMID, NULL);
}

void FC_FUNC(fork,FORK)(int*pid){
    *pid = fork();
}


void FC_FUNC(forkn,FORKN)(int*howmany,int*whoami){
    int i, pid;
    for(i=0;i<*howmany;i++)
    {
        pid = fork();
        if (pid == 0){
            *whoami = i;
            return;
        }
    }
    *whoami = -1;
}


void FC_FUNC(waitall,WAITALL)(){
    pid_t wpid;
    int status = 0;
    while ((wpid = wait(&status)) > 0)
    {
        //printf("Exit status of %d was %d (%s)\n", (int)wpid, status,
        //       (status > 0) ? "accept" : "reject");
    }
    
}

/*
        ! Core properties
        !  Bit 0: If set, will quit ASAP
        !  Bit 1: If set, data is availble to sample
        !  Bit 2: If set, core is sampling data
        !  Bit 3: If set, core is done sampling and has produced results
        !  Bit 8-40: batch_size
*/



int FC_FUNC_(get_batch_size,GET_BATCH_SIZE)
            (shminfo*base, memindex*offset, int*serialnumber)
{
    int batch_size, n;
    long long control;
    long long * pointer;
    n = *offset+*serialnumber;
    pointer = base->addr;
    pointer += n; 
    
    control = *pointer;
    //printf("Control register for worker %d = %lld\n", *serialnumber, control);

    return control >> 8;
}

void FC_FUNC_(set_batch_size,SET_BATCH_SIZE)
            (shminfo*base, memindex*offset, int*serialnumber, int*batch_size)
{
    long long control;
    long long * pointer;
    int i, n;

    n = *offset+*serialnumber;
    //printf("SHM id = %d,offset=%d, %p\n", base->id,n, base->addr);
    pointer = base->addr;
    
    pointer += n;
    
    control = *pointer;
    //printf("Old control register for worker %d = %lld\n", *serialnumber, control);
    
    // Clearing bits 32-63
    control &= 0xff;
    control |= (*batch_size<<8);

    //printf("New control register for worker %d = %lld\n", *serialnumber, control);
    
    *pointer = control;
}


void FC_FUNC_(set_core_property,SET_CORE_PROPERTY)
        (shminfo*base, memindex*offset, int*serialnumber, int*reg, int*v)
{
    long long control;
    long long * pointer;
    int i, n, x;

    x = *reg;

    n = *offset+*serialnumber;
    pointer = base->addr;
    pointer += n;
    
    control = *pointer;
    //printf("Old control register for worker %d = %lld\n", *serialnumber, control);
    if (*v == 0)
        control &= ~(1 << x);
    else if(*v == 1)
        control |= 1 << x;

    //printf("New control register for worker %d = %lld\n", *serialnumber, control);
    
    *pointer = control;
    
}


int FC_FUNC_(get_core_property,GET_CORE_PROPERTY)
        (shminfo*base, memindex*offset, int*serialnumber, int*reg)
{
    long long control;
    long long * pointer;
    int i, n, x;

    x = *reg;

    n = *offset+*serialnumber;
    pointer = base->addr;
    pointer += n;
    
    control = *pointer;
    //printf("Old control register for worker %d = %lld\n", *serialnumber, control);
    
    return (control >> x) & 1;
}
// ulimit -u 709


#ifdef HAVE_SIGNAL_H
#ifdef HAVE_UNISTD_H

void(*killfunction)();
void sigint_handler(int sig) {
    printf("\nCTRL-C detected (signal %d)\n", sig);
    
    // Deregister signal
    signal(sig, SIG_DFL);

    // Call kill functions
    (*killfunction)();

    // Recall signal
    kill(getpid(), sig);
}
#endif
#endif


void FC_FUNC_(register_signal,REGISTER_SIGNAL)(void(*r)())
{
#ifdef HAVE_SIGNAL_H
#ifdef HAVE_UNISTD_H
    killfunction = r;

    signal(SIGINT , sigint_handler); // ^C
    signal(SIGHUP , sigint_handler); // close of terminal
    signal(SIGQUIT, sigint_handler); /* ^\ */
    signal(SIGILL , sigint_handler); // Illegal command
    signal(SIGABRT, sigint_handler); // call of `abort'
    signal(SIGFPE , sigint_handler); // Floating point error
    signal(SIGSEGV, sigint_handler); // Segment violation
#endif
#endif
}
