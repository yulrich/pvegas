                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  SAMPLING
                 !!!!!!!!!!!!!!!!!!!!!!!
        use global_def, only: prec
        integer*8 f_batch_counter
        integer*8 x_batch_counter
        integer*8 w_batch_counter
        integer*8 hist_data_counter

        real(prec) SHM(2)
        integer*8 SHMoffset

        integer*8 control_offset

        
        integer get_core_property,get_batch_size
        external get_core_property,get_batch_size
      contains


      subroutine sequential_sample(batch_size, wgt_batch, x_batch, f_batch, fxn, ndim)
        use global_def
        implicit real(prec)(a-h,o-z)

        dimension x_batch(maxdim, max_batch_size), wgt_batch(max_batch_size), &
                  f_batch(max_batch_size)


        integer bid, batch_size, ndim

        interface
            function fxn(x,wgt,ndim)
                use global_def,only: prec
                integer :: ndim
                real(prec), dimension(ndim) :: x
                real(prec) :: wgt, fxn
            end function fxn
        end interface

        running_parallel = .false.

        do bid = 1,batch_size
            if ( wgt_batch(bid).ge.0.) then
                f_batch(bid) = fxn( x_batch(1:ndim, bid),  wgt_batch(bid) , ndim) & 
    &                             * wgt_batch(bid)
            else
                goto 100
            endif
        enddo
100     batch_size = bid


      end subroutine sequential_sample


! This is parallel. Everything else isn't!
      



      subroutine hinit(ndim, fxn)
        use global_def
        implicit none
        integer ndim
        integer i,n, stat, serialnumber
        
        interface
            function fxn(x,wgt,ndim)
                use global_def,only: prec
                integer :: ndim
                real(prec), dimension(ndim) :: x
                real(prec) :: wgt, fxn
            end function fxn
        end interface
        

        character(len=255) :: var
        call getenv("VEGASCORES", var)
        read(var,*,iostat=stat) i

        if(stat.ge.0) then
            max_threads = i
        else
            max_threads = 2
        endif
        
        ! The reals f_batch, w_batch, x_batch and hist_data need `sizeofreal` 
        ! bytes. The control base needs 8 bytes but SHM must be defined as
        ! real(prec) otherwise FORTRAN does not work properly. The size given
        ! to shmalloc must therefore be `sizeofreal`. 
        ! To have a reals and b integers the total number of reals requested
        ! must be         __                   __
        !                |            8          |
        !         n =    |  a + ------------ b   |
        !                |       sizeofreal      |
        !
        ! The first of the b integers is therefore at the
        !                        __                __
        !                       |     sizeofreal     |
        !      control_offset = |    ------------  a |
        !                       |         8          |
        !
        ! long long, i.e. long long * SHM[control_offset] in C.

        n =   max_batch_size                            & ! f_batch
            + max_batch_size                            & ! w_batch
            + ndim*max_batch_size                       & ! x_batch
            + maxbins*maxhist                           & ! hist data
            + ceiling(8._prec*max_threads/sizeofreal)     ! Control base

        control_offset = ceiling((max_batch_size        & ! f_batch
                                + max_batch_size        & ! w_batch
                                + ndim*max_batch_size   & ! x_batch
                                + maxbins*maxhist       & ! hist data
                ) * sizeofreal / 8._prec) 
        
        
        call shmalloc(SHM, SHMoffset, n, sizeofreal)
        call register_signal(hfree)
        
        f_batch_counter   = SHMoffset
        w_batch_counter   = f_batch_counter + max_batch_size
        x_batch_counter   = w_batch_counter + max_batch_size
        hist_data_counter = x_batch_counter + max_batch_size*ndim

        !call shmalloc(control_base, control_counter, max_threads, 8)
        
        do n=1,maxhist
            do i=1,maxbins
                SHM(hist_data_counter+(n-1)*maxbins+i) = 0
            enddo
        enddo
        do i = 1,max_batch_size
            SHM(f_batch_counter+i) = 0d0
            SHM(w_batch_counter+i) = 0d0
        enddo

        do n=1,max_batch_size
            do i=1,ndim
                SHM(x_batch_counter+(n-1)*ndim+i) = 0
            enddo
        enddo

!        do i=1,max_threads
!            control_base(control_counter+i) = 0
!        enddo


        

        call forkN(max_threads, serialnumber)

        if ( serialnumber.ge.0 ) call core_master(serialnumber, ndim, fxn)        
      end subroutine

      subroutine hfree
        use global_def
        implicit none
        integer i
        do i=0,max_threads-1
            call set_core_property(SHM,control_offset,i,0,1)
        enddo
        call waitAll

        call shmfree(SHM)
!        call shmfree(control_base)
      end subroutine
      


      subroutine core_master(serialnumber, ndim, fxn)
        ! If running parallel, hinit forks. The main thread continues 
        ! in pvegas.f95 and all children enter this routine. They are
        ! kept spinning until they are shut down explicitly. 
        use global_def
        implicit none
        integer serialnumber, ndim
        integer batch_size, bid
        real(prec) x(ndim), wgt, F

        interface
            function fxn(x,wgt,ndim)
                use global_def,only: prec
                integer :: ndim
                real(prec), dimension(ndim) :: x
                real(prec) :: wgt, fxn
            end function fxn
        end interface

        !print*, "Starting core.. Standing by for instructions",ndim
        do while(get_core_property(SHM,control_offset,serialnumber, 0).eq.0)
            if (get_core_property(SHM,control_offset,serialnumber, 1).eq.1) then
                if (get_core_property(SHM,control_offset,serialnumber, 3).eq.1) then
                    cycle
                endif
                ! Start sampling data
                call set_core_property(SHM,control_offset,serialnumber, 1, 0)  ! Accepted job
                call set_core_property(SHM,control_offset,serialnumber, 2, 1)  ! State = sampling
                call set_core_property(SHM,control_offset,serialnumber, 3, 0)  ! No output ready
                
                batch_size = get_batch_size(SHM,control_offset,serialnumber)
!                print*,"Start sampling on ",serialnumber,batch_size
                do bid = 1,batch_size
                    if (MOD(bid, max_threads).ne.serialnumber) cycle
                    
                    x = SHM(                       &
                        x_batch_counter+(bid-1)*ndim+1:     &
                        x_batch_counter+(bid-1)*ndim+ndim   &
                    )
                    wgt = SHM(w_batch_counter+bid)

                    F = fxn(x,wgt,ndim) * wgt
                    SHM(f_batch_counter+bid) = F
                enddo
                 
                call set_core_property(SHM,control_offset,serialnumber, 2, 0)  ! State = idle
                call set_core_property(SHM,control_offset,serialnumber, 3, 1)   ! Output ready
!                print*,'Stop on',serialnumber
            endif
        enddo
        !print*, "Stopping core"
        call EXIT(0)
      end subroutine

      subroutine parallel_sample(batch_size, wgt_batch, x_batch, f_batch, ndim)
        use global_def
        implicit real(prec)(a-h,o-z)
        logical done

        dimension x_batch(maxdim, max_batch_size), wgt_batch(max_batch_size), &
                  f_batch(max_batch_size)


        integer bid, batch_size,  serialnumber, i, ndim
        running_parallel = .true.

        do bid = 1,batch_size
            if ( wgt_batch(bid).lt.1e-50_prec) then
                goto 100
            endif
            
            do i=1,ndim
                SHM(x_batch_counter+(bid-1)*ndim+i) = x_batch(i,bid)
            enddo    
            SHM(w_batch_counter+bid) = wgt_batch(bid)
        enddo
        batch_size = bid
        goto 200
100     continue
        batch_size = bid - 1
200     continue
        
        
        do serialnumber=0,max_threads-1
            call set_batch_size(SHM,control_offset,serialnumber, batch_size)
            call set_core_property(SHM,control_offset,serialnumber, 1, 1)
            call set_core_property(SHM,control_offset,serialnumber, 3, 0)
        enddo
        
        done = .false.
        do while (.not.done)
            done = .true.
            do serialnumber=0,max_threads-1
                done = done.and.(get_core_property(SHM,control_offset,serialnumber,3).eq.1)
            enddo
        enddo
        do bid=1,batch_size
            f_batch(bid) = SHM(f_batch_counter+bid)
        enddo

        do serialnumber=0,max_threads-1
            call set_core_property(SHM,control_offset,serialnumber, 3, 0)
        enddo



      end subroutine parallel_sample

                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  SAMPLING
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

