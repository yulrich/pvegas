



                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  PVEGAS
                 !!!!!!!!!!!!!!!!!!!!!!!
    use distribution, only: setup_histogram, add_shot, setup_histogram2, add_shot2,  &
     write_to_file, write_to_unit
    use global_def, only: prec
  contains

 
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Copyright (c) 1976/1988/1995 G.P. Lepage, Cornell University c
! Permission is granted for anyone to use vegas software       c
! for any purpose on any computer. The author is not           c
! responsible for the consequences of such use. The            c
! software is meant to be freely distributed.                  c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!    <<<<<<<<<<< CHANGED VERSION Adrian Signer  >>>>>>>>>>>>>
!    <<<<<<<<<<< CHANGED VERSION Yannick Ulrich >>>>>>>>>>>>>


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! the following routines are essential to vegas()
!
! N.B. Integrals up to dimension "maxdim" can be done with the code 
!     currently maxdim = 17 :: if maxdim is changed
!     explicit change  x(17) -> x(??) needed in line 64   
 

      subroutine vegas(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename,flag)
!
!   ndim-dimensional Monte Carlo integration of fxn
!        - gp lepage  sept 1976/(rev)aug 1979  (j comp phys 27,192(1978))
!
!   arguments:
!       ndim     = no of dimensions
!       ncall    = number of fxn evaluations per iteration
!       itmx     = number of iterations
!       fxn      = integrand f(x,wgt) where x(i)=integ'n pt (can ignore wgt)
!       avgi     = Monte Carlo estimate of integral
!       sd       = error estimate for answer
!       chi2a    = chi^2/dof test that different iterations agree 
!                  (should be of order 1 or less)
!       randy    = Random number seed
!       filename = statefile
!       flag     = bit 0-2: verbosity (0: no output, 1: runtime info, 
!                                2: detailed info, 3: full verbose)
!                  bit 3: Sequential sampling
!                  bit 4: Retain statefile
!                  bit 5-6: What to read from stae file
!                       0: Everything
!                       1: Only grid
!                       2: Nothing
!                  bit 7: If set, no statefile will be written (!)
!
!   implicit arguments (via common block; all have default values):
!       xl(i) = lower limit of integration in direction i
!       xu(i) = upper limit of integration in direction i
!       acc   = accuracy desired
!
  use global_def
  use sampling
  use vegas_io
  use distribution, only: accumulate, finalise
  use distribution, only: val, sumsq

      implicit real(prec)(a-h,o-z)
      integer batch_size
      
!==!      common/bveg1/ndev,xl(maxdim),xu(maxdim),acc
!==!      common/bveg2/it,ndo,si,swgt,schi,xi(50,maxdim)
!==!      common/bveg3/alph,ndmx,mds
      dimension d(50,maxdim),di(50,maxdim),xin(50),r(50),dx(maxdim), &
                ia(maxdim),kg(maxdim),dt(maxdim),                    &
                x(maxdim),xl(maxdim),xu(maxdim), xi(50,maxdim)
      dimension x_batch(maxdim, max_batch_size), wgt_batch(max_batch_size), &
                f_batch(max_batch_size), ia_batch(maxdim, max_batch_size)
      integer randy, bid, fflag, dummy
!      real ran2
      real(prec) smth
      CHARACTER(len=*), optional :: filename
      integer, optional :: flag

        interface
            function fxn(x,wgt,ndim)
                use global_def,only: prec
                integer :: ndim
                real(prec), dimension(ndim) :: x
                real(prec) :: wgt, fxn
            end function fxn
        end interface



!      external fxn
      data smth/2/

!  initiallize non needed random variables to any value
      
!      do i= ndim+1,12
!         x(i) = 0.3d0
!      enddo

!
!  I've put the default assignments which will NOT be overridden here
!

      do i = 1,maxdim
        xl(i) = 0.d0
        xu(i) = 1.d0
        do ii = 1,50
          xi(ii,i) = 1.d0
        enddo
      enddo

      alph = 1.5d0
      ndmx = 50
      mds = 1
      ndev = 6
      ndo = 1
      it = 0
      si = 0.d0
      swgt = 0.d0
      schi = 0.d0
      acc = -1.d0
!
!     end default assignments
!
      ndo=1
      do j=1,ndim
        xi(1,j)=1.d0
      enddo
!
      entry vegas1(ndim,ncall,itmx,fxn,avgi,sd,chi2a,randy,filename, flag)
!         - initializes cummulative variables, but not the grid
      val = 0.
      sumsq = 0.


      it=0
      si=0.d0
      swgt=si
      schi=si
!
!         - no initialization
      nd=ndmx
      ng=1
      if(mds.eq.0) go to 2
      ng=(ncall/2.)**(1./ndim)
      mds=1
      if((2*ng-ndmx).lt.0) go to 2
      mds=-1
      npg=ng/ndmx+1
      nd=ng/npg
      ng=npg*nd
 2    k=ng**ndim
      npg=ncall/k
      if(npg.lt.2) npg=2
      calls=npg*k
      dxg=1d0/ng
      dv2g=(calls*dxg**ndim)**2/npg/npg/(npg-1d0)
      xnd=nd
      ndm=nd-1
      dxg=dxg*xnd
      xjac=1./calls
      do j=1,ndim
        dx(j)=xu(j)-xl(j)
        xjac=xjac*dx(j)
      enddo
!
!   rebin, preserving densities
      if(nd.eq.ndo) go to 8
      rc=ndo/xnd
      do  j=1,ndim
        k=0
        xn=0
        dr=xn
        i=k
 4      k=k+1
        dr=dr+1.
        xo=xn
        xn=xi(k,j)
 5      if(rc.gt.dr) go to 4
        i=i+1
        dr=dr-rc
        xin(i)=xn-(xn-xo)*dr
        if(i.lt.ndm) go to 5
        do  i=1,ndm
         xi(i,j)=xin(i)
        enddo
        xi(nd,j)=1.d0
      enddo
      ndo=nd
!
 8    continue
! if(nprn.ge.0) write(ndev,200) ndim,calls,it,itmx,acc,nprn, &
!                          alph,mds,nd,(j,xl(j),j,xu(j),j=1,ndim)
!
!         - main integration loop

      sizeofreal = sizeof(smth)
      if (present(flag)) then
        fflag = flag
      else
        fflag = 1
      endif

      if (AND(fflag, 3) .ge. 2) then
        write(*,"(A)") "VEGAS input parameters:"
        write(*,"(A,I10,A,I10,A,I10)") '  itmx * ncall:', itmx,' * ',ncall,' = ',itmx*ncall
        write(*,"(A,I2)") '  ndim:', ndim
        write(*,"(A,I10)") '  seed:', randy
        if (present(filename)) write(*,"(A,A)") '  statefile: ', filename
        print*,
      endif

      if (AND(fflag, 3) .ge. 3) then
        write(*,"(A)") "pVEGAS was compiled with the following options"
        write(*,"(A,I10)") " Maximal number of dimensions = ", maxdim
        write(*,"(A,I10)") " Batch size = ", max_batch_size
        write(*,"(A,I10)") " Maximal bins / histogram = ", maxbins
        write(*,"(A,I10)") " Maximal number of histograms = ", maxhist
        write(*,"(A,I3,A,I3,A)")" Precision kind ",prec," corresponds to ",sizeofreal," bytes"
      endif
      

      if (AND(fflag, 8).eq.0) then
        call hinit(ndim, fxn)
      endif
    
      if (present(filename)) then
          if (AND(fflag, 96).eq.0) then
              call read_grid(filename, ndim, it, ndo, si, swgt, schi, xi)
              if (it.gt.0 .and. AND(fflag, 3) .ge. 2) then
                  avgi=si/swgt
                  sd=sqrt(1./swgt)
                  print*,"Recovered statefile, starting from:"

                  write(*,"(A,I4,A,ES12.5,A,ES12.5,A,F7.3,A)") "Iteration",it,":",avgi," +/- ",sd," (",100*sd/avgi,"% rel.)"
              endif
          else if (AND(fflag, 96).eq.32) then
              call read_grid(filename, ndim, it, dummy, si, swgt, schi, xi)
              it=0
              si=0._prec
              swgt=0._prec
              schi=0._prec
              val = 0._prec
              sums = 0._prec
              if (AND(fflag, 3) .ge. 2) print*,"Recovered grid from statefile"
          else if (AND(fflag, 96).eq.64) then
              if (AND(fflag, 3) .ge. 2) print*,"Not reading statefile"
          endif
      endif
      do while (it.lt.itmx)
          it = it+1
          ti=0.
          tsi=ti
          do  j=1,ndim
            kg(j)=1
            do  i=1,nd
              d(i,j)=ti
              di(i,j)=ti
            enddo
          enddo
    

        x_batch = 0.
        wgt_batch = -1.
        f_batch = 0.
        ia_batch = 0
        ! Figure out where to throw points, i.e. computing x
        bid = 1
        iold = 1
        do i = 1,ng**ndim
            do j = 1,ndim
                kg(ndim-j+1) = mod( (i-1)/(ng**(j-1)), ng) + 1
            enddo
            do k=1,npg
                wgt=xjac
                do  j=1,ndim
                    xn=(kg(j)-ran2(randy))*dxg+1.
                    ia(j)=xn
                    ia_batch(j, bid) = xn
                    if(ia(j).gt.1) then
                        xo=xi(ia(j),j)-xi(ia(j)-1,j)
                        rc=xi(ia(j)-1,j)+(xn-ia(j))*xo
                    else
                        xo=xi(ia(j),j)
                        rc=(xn-ia(j))*xo
                    endif
                    x(j)=xl(j)+rc*dx(j)
                    wgt=wgt*xo*xnd
                enddo
                x_batch(:, bid) = x
                wgt_batch(bid) = wgt
                bid = bid+1

            enddo


            if ( (bid.gt.max_batch_size-npg).or. (i == ng**ndim) ) then
                batch_size = bid

                if (AND(fflag, 3) .ge. 3) then
                    print*, "Sampling",batch_size,"points"
                endif
                
                if (AND(fflag, 8).ne.0  .or. disable_parallel.eq.1) then
                    call sequential_sample(batch_size, wgt_batch, x_batch, f_batch, fxn, ndim)
                else
                    call parallel_sample(batch_size, wgt_batch, x_batch, f_batch, ndim)
                endif
                bid = 1
                
                ! Post process stuff (do this sequential)
                i2 = iold
                do while (bid.lt.batch_size)
                    do j = 1,ndim
                        kg(ndim-j+1) = mod( (i2-1)/(ng**(j-1)), ng) + 1
                    enddo
                    
                    fb=0.
                    f2b=fb
                    do k = 1,npg
                        f=f_batch(bid)
                        f2=f*f
                        fb=fb+f
                        f2b=f2b+f2
                        do  j=1,ndim
                            di(ia_batch(j, bid),j)=di(ia_batch(j, bid),j)+f
                            if(mds.ge.0) d(ia_batch(j, bid),j)=d(ia_batch(j, bid),j)+f2
                        enddo

                        bid = bid + 1
                    enddo

                    f2b=sqrt(f2b*npg)
                    f2b=(f2b-fb)*(f2b+fb)
                    ti=ti+fb
                    tsi=tsi+f2b
                    if(mds.lt.0) then
                        do  j=1,ndim
                            d(ia_batch(j, bid-1),j)=d(ia_batch(j, bid-1),j)+f2b
                        enddo
                    endif

                    i2 = i2 + 1

                enddo

                


                x_batch = 0.
                wgt_batch = -1.
                f_batch = 0.
                ia_batch = 0
                iold = i
                bid = 1
            endif
            
        enddo
    !
    !   compute final results for this iteration
          tsi=tsi*dv2g
          ti2=ti*ti
          wgt=1./tsi
          si=si+ti*wgt
          swgt=swgt+wgt
          schi=schi+ti2*wgt
          avgi=si/swgt
          chi2a=(schi-si*avgi)/(it-.9999)
          sd=sqrt(1./swgt)
          tsi=sqrt(tsi)

          if (AND(fflag, 3) .ge. 1) then
              write(*,"(A,I4,A,ES12.5,A,ES12.5,A,F7.3,A)") "Iteration",it,":",avgi," +/- ",sd," (",100*sd/avgi,"% rel.)"
          endif
          call accumulate
          
    !
    !
    !   refine grid
          do  j=1,ndim
            xo=d(1,j)
            xn=d(2,j)
            d(1,j)=(smth*xo+xn)/(smth+1)
            dt(j)=d(1,j)
            do  i=2,ndm
              d(i,j)=xo+smth*xn
              xo=xn
              xn=d(i+1,j)
              d(i,j)=(d(i,j)+xn)/(smth+2)
              dt(j)=dt(j)+d(i,j)
            enddo
          d(nd,j)=(xo+smth*xn)/(smth+1)
            dt(j)=dt(j)+d(nd,j)
          enddo
    !
          do  j=1,ndim
          rc=0.
          do  i=1,nd
            r(i)=0.
            if(d(i,j).gt.0.) then
                xo=dt(j)/d(i,j)
                r(i)=((xo-1.)/xo/log(xo))**alph
            endif
            rc=rc+r(i)
          enddo
          rc=rc/xnd
          k=0
          xn=0.
          dr=xn
          i=k
 25       k=k+1
          dr=dr+r(k)
          xo=xn
          xn=xi(k,j)
 26       if(rc.gt.dr) go to 25
          i=i+1
          dr=dr-rc
          xin(i)=xn-(xn-xo)*dr/r(k)
          if(i.lt.ndm) go to 26
            do  i=1,ndm
              xi(i,j)=xin(i)
            enddo
            xi(nd,j)=1.
          enddo
          if (AND(fflag, 128).eq.0  .and.  present(filename) ) then
            call save_grid(filename, ndim, it, ndo, si, swgt, schi, xi)
          endif
                    

!
      enddo
      if (AND(fflag, 16).eq.0) then
        if (present(filename)) call delete_grid(filename)
      endif
      call finalise(itmx)

!
      if (AND(fflag, 8).eq.0) then
        call hfree
      endif

      return
      end subroutine vegas
   









                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  PVEGAS
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

