                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  VEGAS_IO
                 !!!!!!!!!!!!!!!!!!!!!!!
        
      contains
        
        subroutine save_grid(filename, ndim, it, ndo, si, swgt, schi, xi)
            use global_def
            use distribution, only: val, sumsq, initialised,    &
                uppers, lowers, deltas, uppersY, lowersY, deltasY, titles
            implicit none
            integer it, ndo, ndim, ios
            real(prec) si,swgt, schi, xi(50,maxdim)
            CHARACTER(len=*) filename
            open(                                               &
                unit=8, file=filename,                          &  
                action='WRITE', form="UNFORMATTED",             &
                ERR=997,IOSTAT=ios                              &
            )

            write(8,ERR=998) ndim         ! # of dimensions
            write(8,ERR=998) it           ! iteration
            write(8,ERR=998) ndo          ! number of subdivision
            write(8,ERR=998) si           ! Sum S[alpha]   / sigma[alpha]^2
            write(8,ERR=998) swgt         ! Sum 1          / sigma[alpha]^2
            write(8,ERR=998) schi         ! Sum S[alpha]^2 / sigma[alpha]^2
            write(8,ERR=998) xi(:,1:ndim) ! Grid

            ! Save plots
            write(8,ERR=998) initialised
            write(8,ERR=998) val  (:, 1:initialised) ! Sums
            write(8,ERR=998) sumsq(:, 1:initialised) ! Sum of squares
            ! Save plot configs
            write(8,ERR=998) uppers(1:initialised)
            write(8,ERR=998) lowers(1:initialised)
            write(8,ERR=998) deltas(1:initialised)
            write(8,ERR=998) titles(1:initialised)

            write(8,ERR=998) uppersY(1:initialised)
            write(8,ERR=998) lowersY(1:initialised)
            write(8,ERR=998) deltasY(1:initialised)


            

            close(unit=8, ERR=999)
            return

 997        print*, 'Error opening file', ios
            return
 998        print*, 'Error writing file'
            return
 999        print*, 'Error closing file'
            return 

        end subroutine save_grid


        subroutine read_grid(filename, ndim, it, ndo, si, swgt, schi, xi)
            use global_def
            use distribution, only: val, sumsq, initialised

            implicit none
            integer it, ndo, ndim, file_ndim, ios
            real(prec) si,swgt, schi, xi(50,maxdim)
            CHARACTER(len=*) filename
            
            open(                                                       &
                unit=8, file=filename,                                  & 
                STATUS='OLD', action='READ', form="UNFORMATTED",        &
                ERR=997, IOSTAT=ios                                     &
            )
            read(8,ERR=998) file_ndim    ! # of dimensions

            if (file_ndim.ne.ndim) then
                print*,"File does not match request. Abort reading"
                goto 990
            endif


            read(8,ERR=998) it           ! iteration
            read(8,ERR=998) ndo          ! number of subdivision
            read(8,ERR=998) si           ! Sum S[alpha]   / sigma[alpha]^2
            read(8,ERR=998) swgt         ! Sum 1          / sigma[alpha]^2
            read(8,ERR=998) schi         ! Sum S[alpha]^2 / sigma[alpha]^2
            read(8,ERR=998) xi(:,1:ndim) ! Grid

            ! Read plots
            read(8,ERR=998) initialised


            if (file_ndim.ne.ndim) then
                print*,"File's plots do not match request. Abort reading"
                goto 990
            endif
             

            read(8,ERR=998) val  (:, 1:initialised) ! Sums
            read(8,ERR=998) sumsq(:, 1:initialised) ! Sum of squares


 990        continue
            close(unit=8, ERR=999)
            return

 997        continue
            return
 998        print*, 'Error reading file'
            return
 999        print*, 'Error closing file'
            return 

        end subroutine


        subroutine delete_grid(filename)
            CHARACTER(len=*) filename
            integer ios
            open(unit=8, iostat=ios, file=filename, status='old', ERR=997)
            close(8, status='delete')
 997        continue
        end subroutine
                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  VEGAS_IO
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

