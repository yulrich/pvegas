      PROGRAM HELLO
        use pvegas
        use vegas_m, only: oldvegas=>vegas,oldvegas1=>vegas1
        implicit none
        real(prec) avgi,sd,chi2a, integrand, avgi2, sd2, chi2a2
        integer ranseed
      

        call setup_histogram(1,1._prec,0._prec,0.05_prec)
        call setup_histogram2(2, 1._prec, 1._prec, 0._prec, &
              -1._prec, 0.1_prec,0.05_prec,                 & 
            "xE", "Cos[theta]")


        ranseed = 945945
        call oldvegas(5,10000,10,M2ENN,avgi2,sd2,chi2a2,ranseed)
        call oldvegas1(5,1000000,20,M2ENN,avgi2,sd2,chi2a2,ranseed)


        ranseed = 945945


        ! Preconditioning
        call vegas(5,10000,10,M2ENN,avgi,sd,            &
                chi2a,ranseed,'tet.grid',80)

        ! Running
        call vegas(5,1000000,20,M2ENN,avgi,sd,          &
                chi2a,ranseed,'tet.grid',50)
        

        print*,avgi,sd,chi2a
        print*,avgi2,sd2,chi2a2





        ranseed = 945945
        call oldvegas(11,10000,10,ps6er,avgi2,sd2,chi2a2,ranseed)
        call oldvegas1(11,1000000,20,ps6er,avgi2,sd2,chi2a2,ranseed)


        ranseed = 945945


        ! Preconditioning
        call vegas(11,10000,10,ps6er,avgi,sd,            &
                chi2a,ranseed,'tet.grid',80)

        ! Running
        call vegas(11,1000000,20,ps6er,avgi,sd,          &
                chi2a,ranseed,'tet.grid',50)
        

        print*,avgi,sd,chi2a
        print*,avgi2,sd2,chi2a2



        open(unit=99, file="output.top", status='unknown')

        write(99,"(A)") &
             "(--------------------------------------------------)"
        write(99,"(A)") &
             "(                   mu  --> e nu nu                )"
        write(99,"(A)") &
             "(--------------------------------------------------)"
        
        write(99,"(A,ES12.5,A,ES12.5,A)") &
            "( The width is (", avgi, " +/- ", sd, ") MeV )" 
    
        write(99,"(A)") &
             "(--------------------------------------------------)"
    
        call write_to_unit(99, 1)
        close(unit=99)

        call write_to_file("output.mm")
        call write_to_file("output.gnu",2)
       
       contains
       
      function M2ENN(xx, wt, ndim)
        use pvegas, only:prec
        implicit none
        integer ndim
        real(prec) xx(ndim), wt
        real(prec) p1(4), p2(4), p3(4), p4(4), n(4)
        real(prec) Mm, Me, weight, wg, M2ENN
        real(prec) s13, s24, s3n
        
        parameter(Mm = 105.658372)   ! MeV
        parameter(Me = 0.0       )   ! MeV

        call ps_4(xx(1:5),p1,Mm,p2,Me,p3,0._prec,p4,0._prec,weight)
        
        s13 = s(p1,p3)
        s24 = s(p2,p4)
        
        n = (/ 0, 0, 1, 0/)
        s3n = s(p3, n)
        m2enn = 2*s13*s24 - 2*Mm*s24*s3n
        m2enn = 8.*m2enn

        m2enn = m2enn*weight*0.5/Mm

        wg = m2enn*wt

        call add_shot(1, 2*p2(4)/Mm, wg)
        call add_shot2(2, 2*p2(4)/Mm, COS_TH(p2, n), wg)
      end function

      function ps6er(xx,wt,ndim)
        use pvegas, only: prec
        implicit none
        integer ndim
        real(prec) xx(ndim), wt,Mm,Me, weight, ps6er
        real(prec) p1(4), p2(4), p3(4), p4(4), p5(4), p6(4)


        parameter(Mm = 105.658372)   ! MeV
        parameter(Me = 0.511     )   ! MeV

        call ps_6(xx,p1,Mm,p2,Me,p3,Me,p4,Me,p5,0._prec,p6,0._prec,weight)
        ps6er = weight

      end function
        



      FUNCTION COS_TH(k1,k2)
        use pvegas, only: ki=>prec
        real (kind=ki), intent(in) :: k1(4), k2(4)
        real (kind=ki) ::  mag1, mag2,COS_TH

        mag1 = sqrt(k1(1)**2 + k1(2)**2 + k1(3)**2)
        mag2 = sqrt(k2(1)**2 + k2(2)**2 + k2(3)**2)

        cos_th = sum(k1(1:3)*k2(1:3))/mag1/mag2

        END FUNCTION COS_TH


      subroutine PS_4(ra,q1,m1,q2,m2,oq3,m3,oq4,m4,weight)
        use pvegas, only:prec
        implicit none
        real(prec) ra(5), m1, m2, m3, m4
        real(prec) weight, q1(4), q2(4), q3(4), q4(4)
        real(prec) qq3(4)
        real(prec) oq3(4), oq4(4), minv3
        real(prec) pi
        parameter(pi=3.14159265358979323846)

        integer enough_energy


        q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)
        minv3 = ra(1)*m1
        weight = minv3*m1/pi

        call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
        if(enough_energy == 0) then
            weight=0d0
            return
        endif
        weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi

        call pair_dec(ra(4:5),minv3,q3,m3,q4,m4,enough_energy)
        if(enough_energy == 0) then
            weight=0d0
            return
        endif
        weight = weight*0.125*sq_lambda(minv3**2,m3,m4)/minv3**2/pi

        oq3 = boost_back(qq3, q3)
        oq4 = boost_back(qq3, q4)
      end subroutine PS_4
        
      SUBROUTINE PS_6(ra,q1,m1,q2,m2,q3,m3,q4,m4,q5,m5,q6,m6,weight)
      use pvegas, only:prec
      implicit none 
      real (kind=prec), intent(in) :: ra(11), m1, m2, m3, m4, m5, m6
      real (kind=prec), intent(out) :: weight, q1(4), q2(4), q3(4)
      real (kind=prec), intent(out) :: q4(4), q5(4), q6(4)
      integer :: enough_energy
      real (kind=prec) :: xi2eff, e2, e2max,  cos_th2, sin_th2, phi2
      real (kind=prec) :: cos_phi2, sin_phi2
      real(prec) pi
      parameter(pi=3.14159265358979323846)
      real (kind=prec) :: qq3(4), minv3, qq4(4), minv4, qq5(4), minv5

      q1 = (/ 0._prec, 0._prec, 0._prec, m1 /)
      
      minv3 = ra(1)*m1
      weight = minv3*m1/pi
      call pair_dec(ra(2:3),m1,q2,m2,qq3,minv3,enough_energy)
      if(enough_energy == 0) then
         weight=0._prec
         return
      endif 
      weight = weight*0.125*sq_lambda(m1**2,m2,minv3)/m1**2/pi
     
      minv4 = ra(4)*m1
      weight = weight*minv4*m1/pi 
      call pair_dec(ra(5:6),minv3,q3,m3,qq4,minv4,enough_energy)
      if(enough_energy == 0) then
         weight=0._prec
         return
      endif 
      weight = weight*0.125*sq_lambda(minv3**2,m3,minv4)/minv3**2/pi
     
      minv5 = ra(7)*m1
      weight = weight*minv5*m1/pi 
      call pair_dec(ra(8:9),minv4,q4,m4,qq5,minv5,enough_energy)
      if(enough_energy == 0) then
         weight=0._prec
         return
      endif 
      weight = weight*0.125*sq_lambda(minv4**2,m4,minv5)/minv4**2/pi

      call pair_dec(ra(10:11),minv5,q5,m5,q6,m6,enough_energy)
      if(enough_energy == 0) then
         weight=0._prec
         return
      endif 
      weight = weight*0.125*sq_lambda(minv5**2,m5,m6)/minv5**2/pi

      q5 = boost_back(qq5, q5)
      q6 = boost_back(qq5, q6)

      q4 = boost_back(qq4, q4)
      q5 = boost_back(qq4, q5)
      q6 = boost_back(qq4, q6)

      q3 = boost_back(qq3, q3)
      q4 = boost_back(qq3, q4)
      q5 = boost_back(qq3, q5)
      q6 = boost_back(qq3, q6)

      END SUBROUTINE PS_6



      function BOOST_BACK(rec,mo) result(bb)   !!boosts to cms system
        use pvegas, only:prec
        implicit none
        real(prec) rec(4),mo(4)
        real(prec) cosh_a, energy,  dot_dot,n_vec(3), bb(4)

        energy = rec(4)**2 - rec(1)**2 - rec(2)**2 - rec(3)**2

        if(energy < 1.0E-16) then
            energy = 1.0E-12
        else
            energy = sqrt(energy)
        end if

        cosh_a = rec(4)/energy
        n_vec = - rec(1:3)/energy  ! 1/sinh_a omitted

        dot_dot = sum(n_vec*mo(1:3))  ! \vec{n} \dot \vec{m}

        bb(1:3) =    mo(1:3) + n_vec*(dot_dot/(cosh_a + 1) - mo(4))
        bb(4) = mo(4)*cosh_a - dot_dot

      end function BOOST_BACK



      SUBROUTINE PAIR_DEC(random_array,min,q3,m3,q4,m4,enough_energy)
        use pvegas, only:prec
        implicit none
             !!  q3^3 = m3^2;  q4^2 = m4^2;  (q3+q4)^2 = min^2     !!
        real(prec) pi
        parameter(pi=3.14159265358979323846)
        real(prec) random_array(2),min,m3,m4
        real(prec) q3(4),q4(4)
        integer enough_energy
        real(prec)  pp, e3, e4, phi3, sin_th3, cos_th3, sin_phi3,  cos_phi3

        if(min > m3+m4) then
        enough_energy = 1
        else
        enough_energy = 0
        q3 = 0d0; q4 = 0d0;
        return
        endif

             ! Generate q3 and q4 in rest frame of q3+q4

        e3 = 0.5*(min+(m3**2-m4**2)/min)
        e4 = 0.5*(min+(m4**2-m3**2)/min)
        pp = 0.5*sq_lambda(min**2,m3,m4)/min

        phi3 = 2*pi*random_array(1)
        cos_th3 = 2*random_array(2) - 1d0
        sin_th3 = sqrt(1 - cos_th3**2)
        sin_phi3 = sin(phi3)
        cos_phi3 = cos(phi3)

        q3 =(/pp*sin_th3*cos_phi3,pp*sin_th3*sin_phi3, pp*cos_th3, e3/)
        q4 =(/-pp*sin_th3*cos_phi3,-pp*sin_th3*sin_phi3,-pp*cos_th3,e4/)

      END SUBROUTINE PAIR_DEC



      real(prec) FUNCTION SQ_LAMBDA(ss,m1,m2) !! square root of \lambda(s,m1,m2)
        use pvegas, only:prec
        implicit none
        real(prec) ss,m1,m2

        sq_lambda = (ss - (m1+m2)**2)*(ss - (m1-m2)**2)
        sq_lambda = sqrt(sq_lambda)

      END FUNCTION SQ_LAMBDA



      real(prec)  FUNCTION S(q1,q2)
        use pvegas, only:prec
        implicit none
        real(prec) dot_dot,q1(4),q2(4)
        dot_dot = q1(4)*q2(4) - q1(1)*q2(1) - q1(2)*q2(2) - q1(3)*q2(3)
        S =  2*dot_dot
      END FUNCTION S


    end program
