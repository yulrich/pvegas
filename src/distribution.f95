                 !!!!!!!!!!!!!!!!!!!!!!!
                     MODULE  DISTRIBUTION
                 !!!!!!!!!!!!!!!!!!!!!!!
        use global_def
        use sampling, only: hist_data_counter, SHM
        implicit none

        real(prec) val(maxbins, maxhist), error(maxbins, maxhist)
        real(prec) sumsq(maxbins, maxhist)
        real(prec) hist_data(maxbins, maxhist)
        real(prec) uppers(maxhist), lowers(maxhist), deltas(maxhist)

        real(prec) uppersY(maxhist), lowersY(maxhist), deltasY(maxhist)


        real(prec), dimension (maxhist) :: hint, havg


        character (len = 100), dimension (maxhist) :: titles
        
        integer initialised
        integer hist2Indices(maxhist)
        

      contains

        subroutine setup_histogram(n, upper, lower, delta, title)
            implicit none
            integer n, binsN
            real(prec) upper, lower, delta
            character(len=*), optional :: title
            character(len=100) dummy

            if (present(title)) then
                titles(n) = title
            else
                write(dummy, "(A5,I3)") "var #", n
                titles(n) = dummy
            endif
            
            if (lower.gt.upper) then
                write(*,"(A)") "Upper value is smaller than lower, did you mixed them?"
            endif

            if (delta.lt.0) then
                write(*,"(A)") "Delta must be positive"
            endif

            if (n.gt.maxhist) then
                write(*,"(A,I2,A)"), &
                    "Histogram index",n," is larger than maxhist"
                return
            endif

            binsN = ceiling((upper-lower)/delta)
            if (binsN.gt.maxbins) then
                write(*,"(A,I2,A,I5,A)"), &
                    "Histogram index",n," needs ",binsN,' bins (larger than maxbins)'
                print*,
                return
            endif

            uppers(n) = upper
            lowers(n) = lower
            deltas(n) = delta

            hist_data(:,n) = 0.

            initialised = max(n,initialised)

        end subroutine


        subroutine add_shot(n, x, wgt)
            implicit none
            integer n, bin
            real(prec) x, wgt
            
            if (n.gt.initialised) then
                print*,"Histogram",n,"is not initalised"
            endif
            !OMP CRITICAL
            if ( x > lowers(n) .and. x < uppers(n) ) then
                bin = ceiling( (x-lowers(n)) / deltas(n) )

                if (running_parallel) then
                    SHM(hist_data_counter+(n-1)*maxbins+bin) = &
                        SHM(hist_data_counter+(n-1)*maxbins+bin) + wgt / deltas(n)
                else
                    hist_data(bin, n) = hist_data(bin, n) + wgt / deltas(n)
                endif

            endif
            !OMP END CRITICAL

        end subroutine
    

        subroutine setup_histogram2(n0, uX, uY, lX, lY, dX, dY, ttitleX, ttitleY)
            implicit none
            integer n0, nsub, n
            real(prec) uX, uY, lX, lY, dX, dY
            character(len=*), optional :: ttitleX, ttitleY
            character(len=100) dummy, titleX,titleY

            if (present(ttitleX)) then
                titleX = ttitleX
            else
                write(dummy, "(A5,I3,A)") "var #", n0,'X'
                titleX = dummy
            endif
            if (present(ttitleY)) then
                titleY = ttitleY
            else
                write(dummy, "(A5,I3,A)") "var #", n0,'Y'
                titleY = dummy
            endif
            
            

            nsub = ceiling((uY-lY)/dY)
            if (nsub+n0.ge.maxhist) then
                write(*,"(A,I2,A,I3,A)"), &
                    "Plot index",n,": The configuration needs",nsub,"histograms > maxhist"
                return
            endif

            
            do n=n0,nsub+n0
                if (n.eq.n0)then
                    dummy = titleX
                elseif (n.eq.n0+1)then
                    dummy = titleY
                else
                    write(dummy, "(A,I3)") trim(titleX), n-n0
                endif
                call setup_histogram(n, uX, lX, dX, dummy)
                hist2Indices(n) = n0

                uppersY(n) = uY
                lowersY(n) = lY
                deltasY(n) = dY
                
            enddo
        end subroutine

        subroutine add_shot2(n0, x, y, wgt)
            implicit none
            integer n0, bin
            real(prec) x, y, wgt


            if ( y > lowersY(n0) .and. y < uppersY(n0) ) then
                bin = ceiling( (y-lowersY(n0)) / deltasY(n0) )
                call add_shot(bin+n0-1, x, wgt / deltasY(n0) )
            endif
            


        end subroutine


        subroutine accumulate()
            implicit none
            integer i, j
            real(prec) ent


            do i=1, initialised
                do j = 1,maxbins
                    if (running_parallel) then
                        ent = SHM(hist_data_counter+(i-1)*maxbins+j)
                        SHM(hist_data_counter+(i-1)*maxbins+j) = 0.
                    else
                        ent = hist_data(j,i)
                        hist_data(j,i) = 0.
                    endif

                    val  (j, i) = val  (j, i) + ent
                    sumsq(j, i) = sumsq(j, i) + ent**2
                enddo
            enddo

        end subroutine

        
        subroutine finalise(itmx)
            implicit none
            integer i, j, itmx
            real(prec) xin, avg, x, n

            n = dble(itmx)

!            ! Old 
!            val = val / n
!            sumsq = sumsq / n**2
!            error =  sqrt(abs(n**2 * sumsq - n*val**2) / n / (n-1))
            
            ! New
            val = val / n
            error =  sqrt(abs(sumsq - n*val**2) / n / (n-1))

            do i=1, initialised
                xin = 0.
                do j = 1,maxbins
                    x = lowers(i)+deltas(i)*(real(j,prec)-0.5)
                    
                    xin = xin + val(j,i)
                    avg = avg + val(j,i) * x
                enddo

                hint(i) = xin * deltas(i)
                havg(i) = avg / xin
            enddo

        end subroutine


        subroutine write_to_file(filename, tool)
            implicit none
            integer, optional :: tool
            integer ttool
            character(len=*) filename

            if (present(tool)) then
                ttool = tool
            else
                ttool = 0
            endif

            open(unit=99, file=filename, status='unknown')
            call write_to_unit(99, ttool)
            close(unit=99)
        end subroutine
        

        subroutine write_to_unit(uni, tool)
            implicit none
            integer, optional :: tool
            integer ttool
            
            integer i, uni, last2d

            if (present(tool)) then
                ttool = tool
            else
                ttool = 0
            endif
            
            select case(ttool)
                case(0)
                    write(uni,*) '(* This file was generated with pVEGAS  *)'
                case(1)
                    write(uni,*) '( This file was generated with pVEGAS    )'
                    write(uni,*) '( td -b filename.top                     )'
                    write(uni,*) 'SET DEVICE POSTSCRIPT SIDEWAYS'
                    write(uni,*) 'SET SIZE SIDEWAYS'
                case(2)
                    write(uni,*) '# This file was generated with pVEGAS    #'

            end select
            
            last2d = 0
            do i=1, initialised
                if (hist2Indices(i) .eq. 0) then
                    select case(ttool)
                        case(0) 
                            call mathematica1D(i, uni)                    
                        case(1) 
                            call htop(i,uni)
                        case(2) 
                            call gnu1D(i,uni)
                    end select
                    last2d = 0
                elseif (hist2Indices(i) .ne. last2d) then
                    select case(ttool)
                        case(0) 
                            call mathematica2D(i, uni)                    
                        case(1) 
                            call htop2(i,uni)
                        case(2)
                            call gnu2D(i, uni)
                    end select


                    last2d = hist2Indices(i) 
                endif
            enddo
        end subroutine


! Plotting with Mathematica
        subroutine mathematica1D(N, uni)
            implicit none
            integer N, uni
            integer nbin, j
            real(prec) xmin, xmax, dx

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)

            write(uni,100,advance='no') trim(titles(n))
            nbin = ceiling((xmax-xmin)/dx)
            do j =1, nbin
                WRITE(uni,200) & 
        &                 xmin+dx*(real(j,prec)-0.5),  &
        &                 val(j, n), error(j, n)
            enddo
            j = nbin
            WRITE(uni,300) 
            
100 FORMAT( /1x, &
    &'data["',A,'"] = ImportString["')
200 FORMAT(3X,G13.6,',',3X,G13.6,',',3X,G13.6)
300 FORMAT('"];')
        end subroutine

        subroutine mathematica2D(N, uni)
            implicit none
            integer N, uni
            integer nX, nY, iX, iY
            real(prec) xmin, xmax, dx, ymin, ymax, dy

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)

            ymin = lowersY(n)
            ymax = uppersY(n)
            dy   = deltasY(n)
            

            nX = ceiling((xmax-xmin)/dx)
            nY = ceiling((ymax-ymin)/dy)

            write(uni,100,advance='no') trim(titles(n)), trim(titles(n+1))
            do iY = 1,nY
                do iX = 1,nX
                    write(uni, 200) &
        &               xmin+dx*(real(iX,prec)-0.5),      &
        &               ymin+dy*(real(iY,prec)-0.5),      &
        &               val(iX,n+iY-1), error(iX, n+iY-1)
                enddo
            enddo
            
            write(uni, 300) 
            


            
100 FORMAT( /1x, &
    &'density["',A,'"]["',A,'"]  = ImportString["')
200 FORMAT(3X,G13.6,',',3X,G13.6,',',3X,G13.6,',',3X,G13.6)

300 FORMAT('"];')
        end subroutine


! Plotting with gnuplot
        subroutine gnu1D(N, uni)
            implicit none
            integer N, uni
            integer nbin, j
            real(prec) xmin, xmax, dx

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)

            write(uni,100) trim(titles(n)), plot_prefix//trim(titles(n)), xmin, xmax
            nbin = ceiling((xmax-xmin)/dx)
            do j =1, nbin
                WRITE(uni,200) & 
        &                 xmin+dx*(real(j,prec)-0.5),  &
        &                 val(j, n), error(j, n)
            enddo
            WRITE(uni,300) 


100 FORMAT( /1x, &
    &'set xlabel "',A,'"'/,1X, &
    &'set ylabel "',A,'"'/,1X, &
    &'set xrange [',F10.5,':',F10.5,']'/,1X,&
    &'set key off'/,1X,&
    &'plot "-" using 1:2:($2-$3):($2+$3) with errorbars')
200 FORMAT(3X,3X,G13.6,3X,G13.6,3X,G13.6,3X)
300 FORMAT('EOF'/,1X, &
    &'pause -1  "Hit return to continue"'/,1X, &
    &'reset')
        end subroutine

        subroutine gnu2D(N, uni)
            implicit none
            integer N, uni
            integer nX, nY, iX, iY
            real(prec) xmin, xmax, dx, ymin, ymax, dy

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)

            ymin = lowersY(n)
            ymax = uppersY(n)
            dy   = deltasY(n)
            

            nX = ceiling((xmax-xmin)/dx)
            nY = ceiling((ymax-ymin)/dy)

            write(uni,100) trim(titles(n)), trim(titles(n+1))
            do iY = 1,nY
                do iX = 1,nX
                    write(uni, 200) &
        &               xmin+dx*(real(iX,prec)-0.5),  &
        &               ymin+dy*(real(iY,prec)-0.5),  &
        &               val(iX,n+iY-1)
                enddo
            enddo
            
            write(uni, 300)
            

100 FORMAT( /1x, &
    &'set pm3d at b'/,1X, &
    &'set view map'/,1X, &
    &'set xlabel "',A,'"'/,1X, &
    &'set ylabel "',A,'"'/,1X, &
    'set dgrid 100,100'/,1X, &
    &'splot "-" w pm3d')
200 FORMAT(3X,3X,G13.6,3X,G13.6,3X,G13.6,3X)
300 FORMAT('EOF'/,1X, &
    &'pause -1  "Hit return to continue"'/,1X, &
    &'reset')


        end subroutine

       
! Plotting with topdrawer
        SUBROUTINE HTOP(N, uni)
            implicit none
            character(len=100) title
            character(len=3) scale
            integer N,j,nbin, uni
            real(prec) xmin, xmax, dx

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)
            title = titles(n)

            scale = 'lin'

            write(uni,100)  &
        &         trim(title), trim(title), plot_prefix//trim(title), scale, xmin, xmax

            nbin = ceiling((xmax-xmin)/dx)

            do j =1, nbin
                WRITE(uni,'(3X,G13.6,2(2X,G13.6))') & 
        &                 xmin+dx*(real(j,prec)-0.5),  &
        &                 val(j, n), error(j, n)
            enddo

            write(uni,200)
            write(uni,300) HINT(N),HAVG(N),0d0
            write(uni,400)

100 FORMAT( /1x, &
    &' SET WINDOW Y 2.5 TO 7.'/,1X, &
    &' SET WINDOW X 2.5 TO 10.'/,1X, &
    &' SET SYMBOL 5O SIZE 1.8'/,1X, &
    &' TITLE TOP ','"',A,' distribution"',/1X, &
    &' TITLE BOTTOM ','"',A,'"',/1X, &
    &' TITLE LEFT ','"',A,'"',/1X, &
    &' CASE       ','" G"',/1X, &
    &' SET SCALE Y ',A5,/1X, &
    &' (SET TICKS TOP OFF)   '/1x, &
    &' SET LIMITS X ',F10.5,' ',F10.5,/1X, &
    &' SET ORDER X Y DY ') 
200 FORMAT('   PLOT')
300 FORMAT( /1x, &
    &' BOX 7. 0.75 SIZE 9. 1.5'/,1X, &
    &' SET WINDOW Y 0. TO 2.'/,1X, &
    &' SET TITLE SIZE -1.5'/1X, &
    &' TITLE 2.8 1.2 "INTGRL =',E12.5,'   AVGE =',E12.5, &
    &             '   RMS =',E12.5,'"',/1X, &
    &' SET TITLE SIZE -2')
400 FORMAT('   NEW PLOT')
            END SUBROUTINE



        SUBROUTINE HTOP2(N, uni)
            implicit none
            character(len=100) titleX, titleY
            integer n, uni
            integer nX, nY, iX, iY
            real(prec) xmin, xmax, dx, ymin, ymax, dy
             

            xmin = lowers(n)
            xmax = uppers(n)
            dx   = deltas(n)

            ymin = lowersY(n)
            ymax = uppersY(n)
            dy   = deltasY(n)

            titleX = titles(n)
            titleY = titles(n+1)


            

            write(uni,100)  &
        &         trim(titleX), trim(titleY),trim(titleX), trim(titleY), &
        &         xmin, xmax, ymin, ymax

            nX = ceiling((xmax-xmin)/dx)
            nY = ceiling((ymax-ymin)/dy)
            
            write(uni,'(A)',advance='no') 'for   x ='
            do iX = 1, nX
                WRITE(uni,'(3X,G13.6)',advance='no') & 
        &                 xmin+dx*(real(iX,prec)-0.5)
            enddo
            do iY = 1,nY
                write(uni,200,advance='no') ymin+dy*(real(iY,prec)-0.5)
                do iX = 1,nX
                    WRITE(uni,'(3X,G13.6)',advance='no') val(iX,n+iY-1)
                enddo
            enddo

            write(uni,300)
            write(uni,400) 
            write(uni,500)

100 FORMAT( /1x, &
    &' SET INTENSITY 4'/,1X, &
    &' SET WINDOW Y 2.5 TO 7.'/,1X, &
    &' SET WINDOW X 2.5 TO 10.'/,1X, &
    &' SET SYMBOL 5O SIZE 1.8'/,1X, &
    &' TITLE TOP ','"',A,' v. ',A,'"',/1X, &
    &' TITLE BOTTOM ','"',A,'"',/1X, &
    &' TITLE LEFT ','"',A,'"',/1X, &
    &' CASE       ','" G"',/1X, &
    &' (SET TICKS TOP OFF)   '/1x, &
    &' SET LIMITS X ',F10.5,' ',F10.5,/1X, &
    &' SET LIMITS Y ',F10.5,' ',F10.5,/1X, &
    &' SET SCALE Y LINEAR',/1X, &
    &' SET SCALE X LINEAR',/1X, &
    &'read mesh')
200 FORMAT( /1x, &
    &'for   y =',G13.6,' z =')
300 FORMAT(/1x,&
    &'   SET TEXTURE SOLID',/1X, &
    &'   CONTOUR')
400 FORMAT( /1x, &
    &' BOX 7. 0.75 SIZE 9. 1.5'/,1X, &
    &' SET WINDOW Y 0. TO 2.'/,1X, &
    &' SET TITLE SIZE -1.5'/1X, &
    &' TITLE 2.8 1.2 ""',/1X, &
    &' SET TITLE SIZE -2')
500 FORMAT('   NEW PLOT')
            END SUBROUTINE


                 !!!!!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE DISTRIBUTION
                 !!!!!!!!!!!!!!!!!!!!!!!!!!

