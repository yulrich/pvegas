#!/usr/bin/env python
import sys
import subprocess
import datetime
import re
import os
import fcntl
from sys import platform as _platform

def ipcs(pid):
    proc=subprocess.Popen(["ipcs", "-mp"], stdout=subprocess.PIPE)
    shm = proc.communicate()[0]
    return re.findall("(\d+).+" + str(pid), shm)

def clean_ipcs(mypid):
    shm = ipcs(mypid)
    if len(shm) > 0:
        if _platform == "darwin":
            if raw_input(
                "SHM is still allocated! You want to free it now? [Y/N]"
            ).upper() != 'N':
                for i in shm:
                    subprocess.Popen(['ipcrm','-m',i])

        else:
            if raw_input(
                "SHM is still allocated! You want to free it now? [Y/N]"
            ).upper() != 'N':
                subprocess.Popen(['ipcrm','shm']+shm)


def loadVegasLog(path):
    import numpy as np
    with open(path) as fp:
        txt = fp.read()

    data = []
    for vegas_run in txt.split('Vegas input parameters'):
        r = np.array([
            [
                int(i[0])*3600+int(i[1])*60+int(i[2]), # Time
                float(i[3]), float(i[4])
            ] for i in re.findall(
                "(\d\d\d\d):(\d\d):(\d\d)\ \[1\]\ ([\de\.-]+)\ +\+-\ +([\de\.-]+)", vegas_run
            )
        ])
        if len(r) > 0:
            data.append(r)
    if len(data) == 1:
        return data[0]
    else:
        return data



def async_read(fd):
    # set non-blocking flag while preserving old flags
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    # read char until EOF hit
    try:
        ch = os.read(fd.fileno(), 1)
        # EOF
        return ch
    except OSError:
        # waiting for data be available on fd
        return ""


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='MC wrapper')
    parser.add_argument('-apns', action='store_true')
    parser.add_argument('-msg', default=None)
    parser.add_argument('-log', default=None)
    parser.add_argument(
        '-timer', nargs=1, choices=["none", "output", "console", "everywhere"],
        default='everywhere', help="Display current time in {output, console, everywhere}"
    )

    parser.add_argument("file")


    args = parser.parse_args()
    if type(args.timer) == list:
        args.timer = args.timer[0]
    
    

    logDate = datetime.datetime.now()
    strdate = logDate.strftime("%m_%d_%H_%M")

    if args.log is not None:
        logPointer = open(args.log, "w", 1)
    else:
        logPointer = None
    
    if logPointer == None and args.timer == 'none':
        proc = subprocess.Popen(args.file)
    else:
        proc = subprocess.Popen(args.file, stdout=subprocess.PIPE)
    mypid = proc.pid


    sys.stderr.write("MC has pid " + str(mypid) + ".\n")

    try:
        while proc.poll() == None:
            if not logPointer and args.timer == 'none':
                pass
            else:
                line = ""
                while True:
                    c = async_read(proc.stdout)
                    proc.stdout.flush()
                    line += c
                    if c == '\n' or c == '':
                        break
                if line == "": continue
                
                td = datetime.datetime.now()-logDate
                hours, remainder = divmod(td.seconds, 3600)
                minutes, seconds = divmod(remainder, 60)
                
                now = "%04d:%02d:%02d" % (hours + td.days*24, minutes, seconds)



                if logPointer:
                    if args.timer == "everywhere" or args.timer == "output":
                        logPointer.write(now + " " + line)
                    else:
                        logPointer.write(line)
                    logPointer.flush()
                if args.timer == "everywhere" or args.timer == "console":
                    sys.stdout.write(now + " " + line)
                    sys.stdout.flush()
                else:
                    print line,

        if logPointer:
            logPointer.close()
        
        if args.apns:
            if args.msg:
                subprocess.Popen(['apns', args.msg])
            else:
                subprocess.Popen(['apns', 'Done'])
    
    except KeyboardInterrupt:
        print "Received CRTL-C"
        if logPointer:
            logPointer.close()
    
    clean_ipcs(mypid)
    print "Total time elapsed: " + str( (datetime.datetime.now() - logDate) ) + "s"
